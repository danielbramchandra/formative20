package id.co.nexsoft.formative20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("id.co.nexsoft.model")
public class Formative20Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative20Application.class, args);
	}

}
