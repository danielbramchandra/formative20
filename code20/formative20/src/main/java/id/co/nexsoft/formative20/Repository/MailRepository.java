package id.co.nexsoft.formative20.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.model.Mail;

@Repository
public interface MailRepository extends CrudRepository<Mail, Integer>{
	Mail save(Mail email);
//	List<Mail> saveAll(List<Mail> email);
	List<Mail> findAll();
}
