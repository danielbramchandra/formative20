package id.co.nexsoft.formative20.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.co.nexsoft.formative20.Repository.MailRepository;
import id.co.nexsoft.model.Mail;

@Controller
public class ViewController {
	@Autowired
	MailRepository repo;

	@GetMapping("/")
	public String view() {
		return "index";
	}

	@PostMapping(value = "/addJSON", consumes= "application/json")
	public void addJSON(@RequestBody List<Mail> mails) {
		repo.saveAll(mails);
	}

	@GetMapping("/getData")
	public String getAllData(Model model) {
		model.addAttribute("users", repo.findAll());
		return "result";
	}
}
