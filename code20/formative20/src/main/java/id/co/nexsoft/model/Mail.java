package id.co.nexsoft.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Mail {
	@Id
	int id;
	String name;
	String email;
	String body;
	int postId;
	public Mail() {
	}
	public Mail(int id, String name, String email, String body, int postId) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.body = body;
		this.postId = postId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	
}
